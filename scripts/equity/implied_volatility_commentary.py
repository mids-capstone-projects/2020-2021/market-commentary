from   other.utils import uncertainty_region, uncertainty_significance
from   typing import Tuple, List, Dict
import pandas as pd
import numpy as np
import datetime
import argparse
import os

#@exception_handler
def uncertainty_commentary(start_day: str, end_day: str, df: pd.DataFrame, index: str = 'VIX', threshold: int = 1) -> str:
    
    end_day    = datetime.datetime.strptime(end_day, '%Y-%m-%d')
    start_day  = datetime.datetime.strptime(start_day, '%Y-%m-%d')
    
    df['Change']  = df.iloc[:, 1].pct_change(periods=5)
    mean, std = np.mean(df['Change']), np.std(df['Change'])

    df['Z-Score'] = (df['Change'] - mean)/ std
    qt_1, qt_2, qt_3, qt_4 = df.iloc[:, 1].quantile(0.05), df.iloc[:, 1].quantile(0.4), df.iloc[:, 1].quantile(0.6), df.iloc[:, 1].quantile(0.95)
    
    quartiles = [qt_1, qt_2, qt_3, qt_4]
    
    current = df[df['Dates'] == end_day].iloc[0, 1]
    change  = df[df['Dates'] == end_day].iloc[0, 2]
    zscore  = df[df['Dates'] == end_day].iloc[0, 3]
    
    current_area, current_area_score = uncertainty_region(current, quartiles)
    sign, significance = uncertainty_significance(change, zscore, threshold)
    
    commentary = f'{index} experienced a {significance} {sign} step, moving to {current} by {change*100: .2f}%. '

    if (significance == 'sharp') & (sign == 'upward'):
        commentary  += 'We interpret this as a sign of sentiment to be cooling as investors become increasingly uncertain about the market. '
    elif (significance == 'sharp') & (sign == 'downward'):
        commentary  += 'We interpret this as a sign of sentiment to be improving as implied market volatility decreases. '
    
    prior  = df[df['Dates'] == start_day].iloc[0, 1]
    prior_area, prior_area_score = uncertainty_region(prior, quartiles)
    
    if current_area == prior_area:
        commentary += f'It remains on the {current_area} value region.'
    else:
        commentary += f'It changed from the {prior_area} to {current_area} value region.'
    
    if (current > qt_4) | (current < qt_1):
        commentary += ' We expect a reversal in the upcoming days.' 
    
    implied_volatility_score = current_area_score - prior_area_score
    
    return commentary, implied_volatility_score


def main(start_day: str, end_day: str, threshold: int = 1) -> str:
    VIX = pd.read_csv(os.path.join(os.getcwd(), '..',  'data', 'equity', 'Market Sentiment', 'VIX.csv'))
    VIX['Dates'] =  pd.to_datetime(VIX['Dates'], format='%m/%d/%Y')

    V2X = pd.read_csv(os.path.join(os.getcwd(), '..',  'data', 'equity', 'Market Sentiment', 'VSTOXX.csv'))
    V2X['Dates'] =  pd.to_datetime(V2X['Dates'], format='%m/%d/%Y')

    VIX_comment, us_implied_volatility_score = uncertainty_commentary(start_day=start_day, end_day=end_day, df = VIX, index='VIX',    threshold = threshold)
    V2X_comment, eu_implied_volatility_score = uncertainty_commentary(start_day=start_day, end_day=end_day, df = V2X, index='VSTOXX', threshold = threshold)

    commentary = VIX_comment + ' ' + V2X_comment

    return commentary, us_implied_volatility_score, eu_implied_volatility_score


if __name__=="__main__":

    parser = argparse.ArgumentParser(description='Implied Volatility values in a given week.')
    parser.add_argument('--s', type=str,
                        help='Day for which we want to evaluate implied volatility. Format: yyyy-mm-dd.')
    parser.add_argument('--d', type=str,
                        help='Day for which we want to evaluate implied volatility. Format: yyyy-mm-dd.')
    parser.add_argument('--t', type=int, default = 1,
                        help='Significance threshold for commentary. Format: int.')

    args = parser.parse_args()

    commentary, _, _ = main(args.s, args.d, args.t)
    print(commentary)