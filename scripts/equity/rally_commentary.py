from   other.exception_handler import exception_handler
from   typing import Tuple, List, Dict
from   dateutil.relativedelta import relativedelta
import pandas as pd
import numpy as np
import argparse
import datetime
import os

#Function to check whether there was a rally in the past month
def calculate_rally(df: pd.DataFrame, day: datetime.datetime) -> Tuple[str]:
    
    chg_1 = df[df['Dates'] == day].loc[:, '5D_CHG'].values
    chg_2 = df[df['Dates'] == (day - datetime.timedelta(weeks=1))].loc[:, '5D_CHG'].values
    chg_3 = df[df['Dates'] == (day - datetime.timedelta(weeks=2))].loc[:, '5D_CHG'].values
    chg_4 = df[df['Dates'] == (day - datetime.timedelta(weeks=3))].loc[:, '5D_CHG'].values
    
    changes = (chg_1, chg_2, chg_3, chg_4)

    move  = 'rally' if chg_2[0] > 0 else 'sell-off'
    
    if (np.sign(chg_1) == np.sign(chg_2)) & (np.sign(chg_1) == np.sign(chg_3)):
        current = 1
    else:
        current = 0
    
    if (np.sign(chg_2) == np.sign(chg_4)) & (np.sign(chg_3) == np.sign(chg_4)):
        past = 1
    else:
        past = 0
    
    return move, current, past, changes

#@exception_handler
def main(start_day: str, end_day: str) -> str:
    US = pd.read_csv(os.path.join(os.getcwd(), '..', 'data', 'equity', 'Regions', 'SPX.csv'))
    US['Dates']  = pd.to_datetime(US['Dates'], format='%m/%d/%Y')
    US['5D_CHG'] = US['PX_LAST'].pct_change(periods=5) * 100

    end_day = datetime.datetime.strptime(end_day, '%Y-%m-%d')
    move, current, past, changes = calculate_rally(US, end_day)

    rally_commentary = ''

    if (current==1) & (past==1):
        rally_commentary = f'The {move} we have seen these past few weeks in US Equities continues.' #Potentially mention the drivers in terms of Industries
    elif (current==1) & (past==0):
        rally_commentary = f'US Equities have started to {move} this week.'
    elif (current==0) & (past==1):
        rally_commentary = f'The {move} that we had seen these past few weeks in US Equities has reversed.'
    else:
        rally_commentary = ''
    
    return rally_commentary

if __name__ == '__main__':
    #Parse command line input
    #Arguments: day -> day of the week on which to base analysis
    parser = argparse.ArgumentParser(description='Rally or Sell-Off for a given Timeframe.')
    parser.add_argument('--s', type=str,
                        help='Day for which we want to check if there was a rally or sell-off. Format: yyyy-mm-dd.')
    parser.add_argument('--d', type=str,
                        help='Day for which we want to check if there was a rally or sell-off. Format: yyyy-mm-dd.')
    args = parser.parse_args()

    commentary = main(start_day = args.s, end_day = args.d)
    print(commentary) 