import pandas as pd
import numpy as np
import datetime
import argparse
import os

def rsi_commentary(day: str, df: pd.DataFrame, region: str) -> str:
    commentary = ''
    
    prior_week = day - datetime.timedelta(weeks=1)

    current_day_val = df[df['Dates'] == day].iloc[0, 1]
    prior_week_val  = df[df['Dates'] == prior_week].iloc[0, 1]
    
    if (current_day_val >= 65) & ((prior_week_val>35) & (prior_week_val<65)):
        commentary = f'In {region}, RSI moved from neutral to overbought territory, reaching a 9-day moving average value of {round(current_day_val, 2)}.'
        rsi_score = 1
    elif (current_day_val >= 65) & (prior_week_val<=35):
        commentary = f'In {region}, RSI saw a sharp reversal, moving from oversold to overbought territory and reaching a 9-day moving average value of {round(current_day_val, 2)}.'
        rsi_score = 2
    elif (current_day_val >= 65) & (prior_week_val>=60):  
        commentary = f'In {region}, RSI experienced very little intraweek movement, which remains around overbought territory.'
        rsi_score = 0
    elif ((current_day_val>35) & (current_day_val<65)) & ((prior_week_val>35) & (prior_week_val<65)):
        commentary = f'In {region}, There was some intraweek movement for RSI, which remains put in neutral territory.'
        rsi_score = 0
    elif ((current_day_val>35) & (current_day_val<65)) & (prior_week_val>=65):
        commentary = f'In {region}, RSI moved from overbought to neutral territory, reaching a 9-day moving average value of {round(current_day_val, 2)}.'
        rsi_score = -1
    elif ((current_day_val>35) & (current_day_val<65)) & (prior_week_val<=65):
        commentary = f'In {region}, RSI moved from oversold to neutral territory, reaching a 9-day moving average value of {round(current_day_val, 2)}.'
        rsi_score  = 1
    elif (current_day_val <= 35) & ((prior_week_val>35) & (prior_week_val<65)):
        commentary = f'In {region}, RSI moved from neutral to oversold territory, reaching a 9-day moving average value of {round(current_day_val, 2)}.'
        rsi_score  = -1
    elif (current_day_val <= 35) & (prior_week_val>=65):
        commentary = f'In {region}, RSI saw a sharp reversal, moving from overbought to oversold territory and reaching a 9-day moving average value of {round(current_day_val, 2)}.'
        rsi_score  = 2
    elif (current_day_val <= 35) & (prior_week_val<= 35):
        commentary = f'In {region}, RSI experienced very little intraweek movement, which remains around oversold territory.'    
        rsi_score  = 0
    else:
        commentary = 'Error'
        
    return rsi_score, commentary

def main(day: str) -> str:

    day = datetime.datetime.strptime(day, '%Y-%m-%d')

    #Importing SPX data
    spx_rsi  = pd.read_csv(os.path.join(os.getcwd(), '..',  'data', 'equity', 'SPX.csv')).loc[:, ['Dates', 'RSI_9D']]
    spx_rsi['Dates'] =  pd.to_datetime(spx_rsi['Dates'], format='%m/%d/%Y')
    spx_rsi = spx_rsi[(spx_rsi['Dates']<=day) & (pd.DatetimeIndex(spx_rsi['Dates']).year==day.year)]

    #Importing SXXP data
    sxxp_rsi = pd.read_csv(os.path.join(os.getcwd(), '..', 'data', 'equity', 'SXXP.csv')).loc[:, ['Dates', 'RSI_9D']]
    sxxp_rsi['Dates'] =  pd.to_datetime(sxxp_rsi['Dates'], format='%m/%d/%Y')
    sxxp_rsi = sxxp_rsi[(sxxp_rsi['Dates']<=day) & (pd.DatetimeIndex(sxxp_rsi['Dates']).year==day.year)]

    us_rsi_score, spx_commentary  = rsi_commentary(day = day, df = spx_rsi,  region = 'the US')
    eu_rsi_score, sxxp_commentary = rsi_commentary(day = day, df = sxxp_rsi, region = 'Europe')

    rsi_joint_commentary = spx_commentary + ' ' + sxxp_commentary

    return rsi_joint_commentary, us_rsi_score, eu_rsi_score

if __name__=="__main__":
    #Parse command line input
    #Arguments: day -> day of the week on which to base analysis
    parser = argparse.ArgumentParser(description='RSI value in a given week.')
    
    parser.add_argument('--day', type=str,
                        help='Day for which we want to find top stocks. Format: yyyy-mm-dd.')
    args = parser.parse_args()

    commentary, _, _ = main(args.day)
    print(commentary)