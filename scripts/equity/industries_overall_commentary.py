from   other.industries_generation import american_industries, european_industries
from   other.style import style
import numpy as np
import pandas as pd
import datetime
import argparse
import os

#As of now this only uses US prices to make commentary.
#With time I would include volumes in the commentary.

def industries_commentary(start_day: str, end_day: str, price: pd.DataFrame) -> str:
    end_day = datetime.datetime.strptime(end_day, '%Y-%m-%d')
    assert(end_day.weekday() <= 4), style.RED + '''This function only accepts weekdays. Please enter the correct day in YYYY-MM-DD format.''' + style.END

    start_day = datetime.datetime.strptime(start_day, '%Y-%m-%d')
    assert(start_day.weekday() <= 4), style.RED + '''This function only accepts weekdays. Please enter the correct day in YYYY-MM-DD format.''' + style.END

    columns = list(price.columns[1:])

    #Calculating  difference
    end_day_industries   = price[price['Dates']==end_day]
    start_day_industries = price[price['Dates']==start_day]

    change = (end_day_industries.iloc[:,1:].to_numpy()/start_day_industries.iloc[:,1:].to_numpy() - 1) * 100

    # Create Counts
    count_positive = np.sum(np.where(change>1, 1, 0)) / change.shape[1]
    count_negative = np.sum(np.where(change<-1, 1, 0)) / change.shape[1]
    count_stable   = np.sum(np.where((change>=-1) & (change<= 1), 1, 0)) / change.shape[1]

    if (count_positive >= 0.6) & (count_negative <= 0.2):
        commentary = 'Equities performance was strong across Industries.'
    elif (count_positive >= 0.5) & (count_negative > 0.2):
        commentary = 'Equities strong performance was felt across Industries, but some pockets of weakness remain.'
    elif (count_negative >= 0.6) & (count_positive < 0.2):
        commentary = 'Equities performance was poor across Industries.'
    elif (count_negative >= 0.5) & (count_positive > 0.2):
        commentary = 'Equities poor performance was felt across Industries, but some pockets of strength remain.'
    elif (count_positive >= 0.3) & (count_negative >= 0.3):
        commentary = 'Equities performance diverged across Industries.'
    elif (count_stable >= 0.8):
        commentary = 'Equities performance was stable across Industries, showing little movement in general.'
    elif (count_stable >= 0.5) & (count_positive >=0.2):
        commentary = 'Equities performance was stable across Industries, although a few pockets of strength remain for specific sectors.'
    elif (count_stable >= 0.5) & (count_positive < 0.2):
        commentary = 'Equities performance was stable across Industries, although a few pockets of weakness remain for specific sectors.'
    else:
        commentary = 'There were no significant trends identified across Industries.'

    return commentary

def main(start_day:str, end_day: str) -> str:
    SPX_Price  = american_industries()
    #SXXP_Price = european_industries()

    american_commentary = industries_commentary(start_day, end_day, price=SPX_Price)
    #european_commentary = industries_commentary(day, price=SPX_Price, volume=SPX_Volume)

    return american_commentary #, EU

if __name__=="__main__":
    #Parse command line input
    #Arguments: day -> day of the week on which to base analysis
    parser = argparse.ArgumentParser(description='Overall performance of Equity in a given week.')
    parser.add_argument('--s', type=str,
                        help='Day for which we want to evaluate industries performance. Format: yyyy-mm-dd.')
    parser.add_argument('--d', type=str,
                        help='Day for which we want to evaluate industries performance. Format: yyyy-mm-dd.')
    args = parser.parse_args()

    commentary = main(start_day=args.s, end_day=args.d)
    print(commentary)