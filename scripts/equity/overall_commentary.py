from typing import Tuple, List, Dict
from dateutil.relativedelta import relativedelta
from other.exception_handler import exception_handler
import pandas as pd
import numpy as np
import argparse
import datetime
import os

#@exception_handler
def main(start_day: str, end_day: str, threshold: int = 1):
    #Declaring hyperparameters... for now
    end_day   = datetime.datetime.strptime(end_day, '%Y-%m-%d') #Day for commentary.
    start_day = datetime.datetime.strptime(start_day, '%Y-%m-%d')
    prior_year  = start_day - relativedelta(years=1)

    #Reading necessary files
    US    = pd.read_csv(os.path.join(os.getcwd(), '..', 'data', 'equity', 'Regions', 'SPX.csv'))
    EU    = pd.read_csv(os.path.join(os.getcwd(), '..', 'data', 'equity', 'Regions', 'SXXP.csv'))
    Asia  = pd.read_csv(os.path.join(os.getcwd(), '..', 'data', 'equity', 'Regions', 'HSI.csv'))
    EEM   = pd.read_csv(os.path.join(os.getcwd(), '..', 'data', 'equity', 'Regions', 'EEM.csv'))

    regions = [US, EU, Asia, EEM]

    #Looping through regions to get last month pct change in price
    for region in regions:
        region['Dates'] = pd.to_datetime(region['Dates'], format='%m/%d/%Y')
        region['5D_CHG'] = region['PX_LAST'].pct_change(periods=5) * 100
        region['20_CHG'] = region['PX_LAST'].pct_change(periods=20) * 100


    # Calculate Relative Performance
    #Technique: t-test 
    #Variables: x = 1-week pct_change from present day, mu = mean 1-week pct_change for last year
    #Variables: std = std of pct_change for last year, n = 1 (not used)
    relative_performance = []

    for region in regions:
        actual = region[region['Dates']==end_day]['5D_CHG'].values[0]
        mean   = np.mean(region[region['Dates'] > prior_year]['5D_CHG'])
        std    = np.std(region[region['Dates'] > prior_year]['5D_CHG'])
        z      = (actual - mean)/std
    
        if ((np.abs(z) > threshold) & (np.sign(z)==1)):
            relative_performance.append(np.sign(z))
        elif ((np.abs(z) < threshold) & (np.sign(z)==-1)):
            relative_performance.append(np.sign(z))
        else:
            relative_performance.append(np.sign(z)) 


    #Scoring function - to be moved to utils file
    score = np.sum(relative_performance)

    if score >= 3:
        performance = 'a very strong'
    elif score ==2:
        performance = 'an overall strong'
    elif score >=-1:
        performance = 'a stable'
    elif score ==-2:
        performance = 'an overall weak'
    else:
        performance = 'a very weak'  

    #Create commentary and return it
    overview_commentary = f'Equities had {performance} performance for the period between {start_day.date()} and {end_day.date()}.'
    return overview_commentary

if __name__=="__main__":
    #Parse command line input
    #Arguments: day -> day of the week on which to base analysis, threshold -> threshold of significance
    parser = argparse.ArgumentParser(description='Overall performance of Equity in a given week.')
    parser.add_argument('--s', type=str,
                        help='Day for which we want to evaluate the stock market performance. Format: yyyy-mm-dd.')
    parser.add_argument('--d', type=str,
                        help='Day for which we want to evaluate the stock market performance. Format: yyyy-mm-dd.')
    parser.add_argument('--t', type=int, 
                        help='Significance threshold for commentary. Format: int.')
    args = parser.parse_args()

    commentary = main(start_day = args.s, end_day = args.d)
    print(commentary)