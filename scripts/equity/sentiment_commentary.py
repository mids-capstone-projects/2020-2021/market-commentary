from   other.exception_handler import exception_handler
from equity import implied_volatility_commentary, momentum_commentary, rsi_commentary
import argparse
import os

#Add Europe in Market Sentiment and combine commentary

def market_sentiment(rsi: int, implied_volatility: int, market_momentum: int) -> str:
    if (market_momentum > 0) & (rsi > 0) & (implied_volatility > 0):
        market_sentiment_commentary = 'Market Sentiment was overwhelmingly bullish, with good market momentum and positive movements in implied volatility and relative strength.'
    elif (market_momentum >= 0) & (rsi >= 0) & (implied_volatility >= 0):
        market_sentiment_commentary = 'Market Sentiment was generally bullish, with most key indicators staying put or on the positive.'
    elif (market_momentum <= 0) & (rsi <= 0) & (implied_volatility <= 0):
        market_sentiment_commentary = 'Market Sentiment was generally bearish, with most key indicators staying put or on the negative.'
    elif (market_momentum <= 0) & (rsi <= 0) & (implied_volatility <= 0):
        market_sentiment_commentary = 'Market Sentiment was overwhelmingly bearish, with bad market momentum and negative movements in implied volatility and relative strength.'
    else:
        market_sentiment_commentary = 'Mixed Signals on Market Sentiment as key indicators pointed to opposite directions.'
    
    return market_sentiment_commentary

#@exception_handler
def main(start_day: str, end_day: str) -> str:
    _, us_rsi, _ = rsi_commentary.main(start_day)
    _, us_vol, _ = implied_volatility_commentary.main(start_day=start_day, end_day=end_day, threshold=1)
    _, us_mom = momentum_commentary.main(start_day=start_day, end_day=end_day)

    us_mov = 1 if us_mom == 'up' else -1
    #eu_mov = 1 if eu_mom == 'up' else -1

    us_commentary = market_sentiment(rsi=us_rsi, implied_volatility= us_vol, market_momentum= us_mov)

    return us_commentary
    

if __name__=="__main__":
    #Parse command line input
    #Arguments: day -> day of the week on which to base analysis
    parser = argparse.ArgumentParser(description='Market Momentum in a given week.')
    parser.add_argument('--s', type=str,
                        help='Day for which we want to calculate Market Momentum. Format: yyyy-mm-dd.')
    parser.add_argument('--d', type=str,
                        help='Day for which we want to calculate Market Momentum. Format: yyyy-mm-dd.')
    args = parser.parse_args()

    commentary = main(start_day = args.s, end_day = args.d)
    print(commentary)    