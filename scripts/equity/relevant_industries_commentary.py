from   other.industries_generation import american_industries, european_industries
from   other.style import style
import numpy as np
import pandas as pd
import datetime
import argparse
import os

#Add sign for industries
#Add up/down parameter

def relevant_industries(start_day: str, end_day:str, threshold: int, price: pd.DataFrame) -> str:
    end_day = datetime.datetime.strptime(end_day, '%Y-%m-%d')
    start_day = datetime.datetime.strptime(start_day, '%Y-%m-%d')
    columns = list(price.columns[1:])

    #Calculating  difference
    end_day_industries       = price[price['Dates']==end_day]
    start_day_industries = price[price['Dates']==start_day]

    change = (end_day_industries.iloc[:,1:].to_numpy()/start_day_industries.iloc[:,1:].to_numpy() - 1) * 100

    #Calculate mean and standard deviation
    spx_price_std  = np.std(change)
    spx_price_mean = np.mean(change)

    #Calculate zscores
    spx_price_zscore = (change - spx_price_mean)/spx_price_std

    #Identifying notable movements for industries for z-scores
    notable_pos = np.where(spx_price_zscore >= threshold, 1, 0)
    notable_neg = np.where(spx_price_zscore <= threshold*(-1), 1, 0)

    #Notable positive Values
    index_pos = np.where(notable_pos == 1)[1].tolist()
    notable_pos_names  = [columns[i] for i in index_pos]
    notable_pos_values = change[:, index_pos]

    #Notable negative Values
    index_neg = np.where(notable_neg == 1)[1].tolist()
    notable_neg_names  = [columns[i] for i in index_neg]
    notable_neg_values = change[:, index_neg]

    notable_commentary = ''

    count = 0
    if (len(notable_neg_names) == 0) & (len(notable_pos_names)==0):
        notable_commentary = 'There were no notable movements identified across Industries.'
    elif len(notable_pos_names)==1:
        if notable_pos_values[:, 0][0] > 0:
            notable_commentary = f'''{notable_pos_names[0]}, up by {style.GREEN}{notable_pos_values[:, 0][0]:+.2f}%{style.END}, was the strongest performing Industry. '''
        else:
            notable_commentary = ''
    else:
        for i in range(len(notable_pos_names)):
            if notable_pos_values[:, i][0] > 0:
                notable_commentary += f'{notable_pos_names[i]} ({style.GREEN}{notable_pos_values[:, i][0]:+.2f}%{style.END}), '
                count += 1
        if count > 0:
            notable_commentary += 'had a notably strong performance. '
        else:
            notable_commentary += ''

    count = 0
    if (len(notable_neg_names) == 0) & (len(notable_pos_names)==0):
        notable_commentary = 'There were no notable movements identified across Industries.'
    elif len(notable_neg_names)==1:
        if notable_neg_values[:, 0][0] < 0:
            notable_commentary += f'''{notable_neg_names[0]}, down by {style.RED}{notable_neg_values[:, 0][0]:.2f}%{style.END}, was the weakest performing Industry.'''
        else:
            notable_commentary += ''
    else:
        for i in range(len(notable_neg_names)):
            if notable_neg_values[:, i][0] < 0:
                notable_commentary += f'{notable_neg_names[i]} ({style.RED}{notable_neg_values[:, i][0]:.2f}%{style.END}), '
                count += 1
        if count > 0:
            notable_commentary += 'experienced significantly weak performance.'
        else:
            notable_commentary += ''
    
    return notable_commentary

def main(start_day: str, end_day: str, threshold: int) -> str:
    SPX_Price = american_industries()
    american_commentary = relevant_industries(start_day=start_day, end_day=end_day, threshold=threshold,  price=SPX_Price)
    return american_commentary

if __name__=="__main__":
    #Parse command line input
    #Arguments: day -> day of the week on which to base analysis
    parser = argparse.ArgumentParser(description='Overall performance of Equity in a given week.')
    parser.add_argument('--s', type=str,
                        help='Day for which we want to evaluate industries performance. Format: yyyy-mm-dd.')
    parser.add_argument('--d', type=str,
                        help='Day for which we want to evaluate industries performance. Format: yyyy-mm-dd.')
    parser.add_argument('--t', default=1, type=float,  
                        help="Significance threshold for commentary. Format: int.")
    args = parser.parse_args()

    commentary = main(args.s, args.d, args.t)
    print(commentary)