import pandas as pd
import numpy as np
import datetime
import argparse
import os

def main(start_day: str, end_day: str) -> str:
    #Correctly formatting input from the command line
    start_day = datetime.datetime.strptime(start_day, '%Y-%m-%d')
    end_day = datetime.datetime.strptime(end_day, '%Y-%m-%d')

    #Importing data and filtering columns
    SPX          = pd.read_csv(os.path.join(os.getcwd(), '..',  'data', 'equity', 'SPX.csv'))
    SPX['Dates'] = pd.to_datetime(SPX['Dates'], format='%m/%d/%Y')
    SPX_MOV      = SPX.loc[:, ['Dates', 'PX_LAST', 'MOV_AVG_5D', 'MOV_AVG_50D', 'MOV_AVG_100D']]

    #Creating extra moving average for 200 days and filtering for last year
    SPX_MOV['MOV_AVG_200D'] = SPX_MOV['PX_LAST'].rolling(200).mean().round(2)
    SPX_MOV = SPX_MOV[(SPX_MOV['Dates']<=end_day) & (pd.DatetimeIndex(SPX_MOV['Dates']).year>=(start_day.year-1))]

    #Identifying signals when moving averages cross
    SPX_MOV['Signal'] = 0.0
    SPX_MOV['Signal'] = np.where(SPX_MOV['MOV_AVG_50D'] > SPX_MOV['MOV_AVG_200D'], 1.0, 0.0)
    SPX_MOV['Position'] = SPX_MOV['Signal'].diff()

    week_signal = np.sum(SPX_MOV[(SPX_MOV['Dates']<=end_day) & (SPX_MOV['Dates']>=start_day)]['Position'])

    signal = 'buy' if week_signal > 0 else 'sell'
    signal_commentary = ''

    if week_signal != 0:
        mov_avg_signal = f'The two moving-average indexes crossed this week triggering a {signal} signal.'
    else:
        mov_avg_signal = ''

    SPX_MOV['KEY_DIFF'] = SPX_MOV['MOV_AVG_50D'] - SPX_MOV['MOV_AVG_200D']
    SPX_MOV['CAGR'] = SPX_MOV['KEY_DIFF']/SPX_MOV['KEY_DIFF'].shift(periods=5)

    acceleration = 'up' if SPX_MOV[SPX_MOV['Dates']==end_day]['CAGR'].values[0] > 1 else 'down'
    gap = 'widened' if SPX_MOV[SPX_MOV['Dates']==end_day]['KEY_DIFF'].values[0] > SPX_MOV[SPX_MOV['Dates']==start_day]['KEY_DIFF'].values[0] else 'narrowed'

    mov_avg_acceleration_commentary = ''

    if signal != '':
        mov_avg_accelerating_commentary = f'Market Momentum trended {acceleration}, as the gap between the key 50-day and 200-day moving averages {gap}.'
    else:
        mov_avg_accelerating_commentary = f'No relevant movements on Market Momentum.'

    mov_avg_commentary = mov_avg_accelerating_commentary + ' ' + mov_avg_signal

    return mov_avg_commentary, acceleration

if __name__=="__main__":
    #Parse command line input
    #Arguments: day -> day of the week on which to base analysis
    parser = argparse.ArgumentParser(description='Market Momentum in a given week.')
    parser.add_argument('--s', type=str,
                        help='Day for which we want to evaluate implied volatility. Format: yyyy-mm-dd.')
    parser.add_argument('--d', type=str,
                        help='Day for which we want to evaluate implied volatility. Format: yyyy-mm-dd.')
    args = parser.parse_args()

    commentary, _ = main(args.s, args.d)
    print(commentary)