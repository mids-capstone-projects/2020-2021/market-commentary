from utils import fixed_income
from fixed_income import generate_credit_spreads_commentary, generate_total_returns_commentary, generate_yield_curve_commentary, generate_municipal_bond_turns_commentary

if __name__ == '__main__':
    # Dictionary for indices mapping
    # DICT_PATH = '../data/ids_fixed_income.json'
    
    # print(fixed_income.load_indices(path=DICT_PATH))
    input_date = '2020-09-30'
    cm_yield_curve = generate_yield_curve_commentary(input_date)
    cm_credit_spreads = generate_credit_spreads_commentary(input_date)
    cm_total_returns = generate_total_returns_commentary(input_date)
    cm_municipal_bond_returns = generate_municipal_bond_turns_commentary(input_date)


    print(cm_yield_curve)
    print('')
    print(cm_credit_spreads)
    print('')
    print(cm_total_returns)
    print('')
    print(cm_municipal_bond_returns)