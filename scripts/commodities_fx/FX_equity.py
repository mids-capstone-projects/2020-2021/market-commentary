
def generate_FX_equity(input_date,period = "5D",verbose = False):
    
    import pandas as pd
    import numpy as np
  
    SP500 = pd.read_csv('../data/commodities/SP500.csv').iloc[::-1]
    DXY = pd.read_csv('../data/commodities/DXY.csv').iloc[::-1]
    NASDAQ = pd.read_csv('../data/commodities/NASDAQ.csv').iloc[::-1]
    DIJA = pd.read_csv('../data/commodities/DIJA.csv').iloc[::-1]
    
    percentage_change(SP500)
    percentage_change(DXY)
    percentage_change(NASDAQ)
    percentage_change(DIJA)

    #extract data based on input date
    SP500_1 = SP500[SP500["Date"] == input_date][period].tolist()[0]
    DXY_1 = DXY[DXY["Date"] == input_date][period].tolist()[0]
    NASDAQ_1 = NASDAQ[NASDAQ["Date"] == input_date][period].tolist()[0]
    DIJA_1 = DIJA[DIJA["Date"] == input_date][period].tolist()[0]

    #select the trend and corresponding words
    SP500_2 = "fell" if SP500_1 < 0 else "recovered" if SP500_1 != 0 else "maintained"
    DXY_2= "fell" if DXY_1 < 0 else "recovered" if DXY_1 != 0 else "maintained"
    NASDAQ_2= "fell" if NASDAQ_1 < 0 else "recovered" if NASDAQ_1 != 0 else "maintained"
    DIJA_2= "fell" if DIJA_1 < 0 else "recovered" if DIJA_1 != 0 else "maintained"
    #select verbose options
    
    
    if verbose == False:
        
        main = f'''From {input_date} in the past {period[0]} days, the USD index {DXY_2}. Meanwhile, the SP500 index {SP500_2}, with NASDAQ {NASDAQ_2} and the DIJA {DIJA_2}.'''
    else:
        
        main = f'''From {input_date} in the past {period[0]} days, the USD index {DXY_2} by {abs(DXY_1*100):.2f}%. Meanwhile, the SP500 index {SP500_2} by {SP500_1*100:.2f}%, with NASDAQ {NASDAQ_2} by {NASDAQ_1*100:.2f}% and the DIJA {DIJA_2} by  {DIJA_1*100:.2f}%.'''
    
    return main

def percentage_change(df):  
    df['5D'] = df['Last Price'].pct_change(periods=5)
    df['7D'] = df['Last Price'].pct_change(periods=7)
    df['30D'] = df['Last Price'].pct_change(periods=30)
    
