       
def generate_Sector_comments(input_date,period = "5D",verbose = False):
    

    import pandas as pd
    import numpy as np
    
    ENER = pd.read_csv('../data/commodities/ENERGY.csv').iloc[::-1]
    AGRI = pd.read_csv('../data/commodities/AGRI.csv').iloc[::-1][-275:]
    AGRI = AGRI.iloc[:, : 7]
    METAL = pd.read_csv('../data/commodities/METAL.csv').iloc[::-1]
    DXY = pd.read_csv('../data/commodities/DXY.csv').iloc[::-1]
    
    percentage_change2(ENER)
    percentage_change2(METAL)
    percentage_change2(AGRI)
    percentage_change(DXY)

    
    cm_a = agri(input_date,period,AGRI,DXY,verbose)
    cm_m = metal(input_date,period,METAL,DXY,verbose )
    cm_e = energy(input_date,period,ENER,DXY,verbose)
    
    main = cm_a + " " + cm_m + " " + cm_e
    
    return main
    
    
def agri(date,period,AGRI,DXY,verbose = False): 
    
    C1 = AGRI[AGRI["Date"] == date][f'''C1{period}'''].tolist()[0]
    W1 = AGRI[AGRI["Date"] == date][f'''W1{period}'''].tolist()[0]
    S1 = AGRI[AGRI["Date"] == date][f'''S1{period}'''].tolist()[0]
    DXY_1 = DXY[DXY["Date"] == date][period].tolist()[0]

    
    C1_2 = "fell" if C1 < 0 else "recovered" if C1 != 0 else "maintained"
    W1_2 = "fell" if W1 < 0 else "recovered" if W1 != 0 else "maintained"
    S1_2 = "fell" if S1 < 0 else "recovered" if S1 != 0 else "maintained"
    DXY_2= "fall" if DXY_1 < 0 else "rise" if DXY_1 != 0 else "stable movement"
    
    dic = {C1:"Corn", W1:"Wheat", S1:"Soybeans"}
    temp = max(C1,W1,S1)
    num = dic[temp]
    
    if verbose == False:
        main = f'''Agriculture: {num} emerged as one of the strongest performing assets from {date} to past {period[0]} days. The {DXY_2} in the dollar also likely contributed.'''

    else:
        main =  f'''Agriculture: {num} emerged as one of the strongest performing assets from {date} to past {period[0]} days by {DXY_1*100:.2f}%. The {DXY_2} in the dollar also likely contributed. Wheat {W1_2} by {W1*100:.2f}% and soybeans {S1_2} by {S1*100:.2f}% '''
        
    return main


def energy(date,period,ENER,DXY,verbose = False):
    
    C1 = ENER[ENER["Date"] == date][f'''CO1{period}'''].tolist()[0]
    W1 = ENER[ENER["Date"] == date][f'''CL1{period}'''].tolist()[0]
    N1 = ENER[ENER["Date"] == date][f'''NG1{period}'''].tolist()[0]
    DXY_1 = DXY[DXY["Date"] == date][period].tolist()[0]

        
    C1_2 = "fell" if C1 < 0 else "recovered" if C1 != 0 else "maintained"
    W1_2 = "fell" if W1 < 0 else "recovered" if W1 != 0 else "maintained"
    N1_2 = "fell" if N1 < 0 else "recovered" if N1 != 0 else "maintained"
    DXY_2= "fall" if DXY_1 < 0 else "rise" if DXY_1 != 0 else "stable movement"
    
    dic = {C1:"Brent crude oil", W1:"WTI crude oil", N1:"Nartual gas"}
    temp = max(C1,W1,N1)
    num = dic[temp]
    
    
    if verbose == False:
        main = f'''In the energy sector, {num} emerged as one of the most influential factors as {DXY_2} in the dollar was observed.'''

    else:
        main =  f'''In the energy sector, {num} emerged as one of the strongest performing assets from {date} to past {period[0]} days by {DXY_1*100:.2f}%. The {DXY_2} in the dollar also likely contributed. Brent crude oil {C1_2} by {C1*100:.2f}% and nartual gas {N1_2} by {N1*100:.2f}%'''
        
    return main

def metal(date,period,METAL,DXY,verbose = False):
    
    XAU = METAL[METAL["Date"] == date][f'''XAU{period}'''].tolist()[0]
    XAG = METAL[METAL["Date"] == date][f'''XAG{period}'''].tolist()[0]
    XPT = METAL[METAL["Date"] == date][f'''XPT{period}'''].tolist()[0]
    XPD = METAL[METAL["Date"] == date][f'''XPD{period}'''].tolist()[0]
    HG = METAL[METAL["Date"] == date][f'''HG1{period}'''].tolist()[0]
    DXY_1 = DXY[DXY["Date"] == date][period].tolist()[0]

    
    
    XAU2 = "fell" if XAU < 0 else "recovered" if XAU != 0 else "maintained"
    XAG2 = "fell" if XAG < 0 else "recovered" if XAG != 0 else "maintained"
    XPT2 = "fell" if XPT < 0 else "recovered" if XPT != 0 else "maintained"
    XPD2 = "fell" if XPD < 0 else "recovered" if XPD != 0 else "maintained"
    HG2 = "fell" if HG < 0 else "recovered" if HG != 0 else "maintained"
    DXY_2= "fall" if DXY_1 < 0 else "rise" if DXY_1 != 0 else "stable movement"

    dic = {XAU:"Gold", XAG:"Silver", XPT:"Platinum",HG:"Copper",XPD:"Palladium"}
    dic2 = {XAU:XAU2 , XAG:XAG2, XPT:XPT2,HG:HG2,XPD:XPD2}
    
    temp = max(XAU,XAG,XPT,HG,XPD)
    temp2 = min(XAU,XAG,XPT,HG,XPD)
    num = dic[temp]
    num2 = dic[temp2]


    if verbose == False:
        main =  main =  f'''{num} emerged as one of the strongest performing assets by {temp*100:.2f}%. {num2} {dic2[temp2]} by {temp2*100:.2f}%, which was the worst performing index. The recent {DXY_2} in the dollar last week also likely contributed.'''
       # main =  f'''{num} emerged as one of the strongest performing assets from {date} to past {period[0]} days. The {DXY_2} in the dollar also likely contributed.'''
    else:
        main =  f'''{num} emerged as one of the strongest performing assets from {date} to past {period[0]} days by {temp*100:.2f}%. The {DXY_2} in the dollar also likely contributed. Gold {XAU2} by {XAU*100:.2f}%. Silver {XAG2} by {XAG*100:.2f}%. Platinum {XPT2} by {XPT*100:.2f}%. Copper {HG2} by {HG*100:.2f}% and Palladium {XPD2} by {XPD*100:.2f}%. '''

    return main
    

def percentage_change(df):  
    df['5D'] = df['Last Price'].pct_change(periods=5)
    df['7D'] = df['Last Price'].pct_change(periods=7)
    df['30D'] = df['Last Price'].pct_change(periods=30)
    
def percentage_change2(df): 
    if len(df.columns) >=10:
        for i in df.columns[1:7]:
            df[i + '5D'] = df[i].pct_change(periods=5)
            df[i + '7D'] = df[i].pct_change(periods=7)
            df[i +'30D'] = df[i].pct_change(periods=30)         
    else: 
        for i in df.columns[1:]:
            df[i + '5D'] = df[i].pct_change(periods=5)
            df[i + '7D'] = df[i].pct_change(periods=7)
            df[i +'30D'] = df[i].pct_change(periods=30)
            