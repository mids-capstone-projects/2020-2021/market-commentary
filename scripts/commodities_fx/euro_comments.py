def generate_euro_comments(input_date,period = "5D",verbose = False):
    
    import pandas as pd
    import numpy as np
        
    eurusd = pd.read_csv('../data/commodities/euro.csv').iloc[::-1]
    spx = pd.read_csv('../data/commodities/SPX.csv')
    sxxp = pd.read_csv('../data/commodities/SXXP.csv')
    DXY = pd.read_csv('../data/commodities/DXY.csv').iloc[::-1]
    
    DXY['5D'] = DXY['Last Price'].pct_change(periods=5)
    DXY['7D'] = DXY['Last Price'].pct_change(periods=7)
    DXY['30D'] = DXY['Last Price'].pct_change(periods=30)
    


    EURUSD = eurusd[eurusd["Date"] == input_date]['''EURUSD'''].tolist()[0]
    SPX = spx[spx["Dates"] == input_date][f"CHG_PCT_{period}"].tolist()[0]
    SXXP = sxxp[sxxp["Dates"] == input_date][f"CHG_PCT_{period}"].tolist()[0]
    DXY_1 = DXY[DXY["Date"] == input_date][period].tolist()[0]

    
    EURUSD2 = "fell" if EURUSD < 0 else "appreciated" if EURUSD != 0 else "maintains"
    SPX2 = "fell" if SPX < 0 else "recovered" if SPX != 0 else "maintains"

    SXXP2 = "fall" if SXXP < 0 else "rise" if SXXP != 0 else "stable movement"
    DXY_2= "fall" if DXY_1 < 0 else "rise" if DXY_1 != 0 else "stable movement"


    if verbose == False:
        main  =  f'''From {input_date}, a {SXXP2} in SXXP was observed with a {DXY_2} in USD. SPX also {SPX2} as the euro {EURUSD2}.'''
    else:
        main  =  f'''From {input_date}, a {SXXP2} in SXXP of {SXXP*100:.2f}% was observed with a {DXY_1*100:.2f}% of {DXY_2} in USD. SPX also {SPX2} by {SPX*100:.2f}% while the euro {EURUSD2} by {EURUSD*100:.2f}%.'''

    return main
