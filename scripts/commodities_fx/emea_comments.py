    
def generate_EMEA_comments(input_date ,period = "5D",verbose = False) :
    import pandas as pd
    import numpy as np

    curr = prepare_data()
    #check validity
    if len(curr[curr["Date"] == input_date][period].tolist()) == 0:
        return "No entry related to this date is found. Please retry with valid input"
    #select the date data
    df = curr[curr["Date"] == input_date][period].tolist()[0]
    #select the trend and corresponding words
    df_2 = "fell" if df < 0 else "recovered" if df != 0 else "maintained"
    #select verbose options
    if verbose == False:
        main  =  f'''From {input_date} in the past {period[0]} days, the index of USD against EMEA currencies {df_2:s}.'''
    else:
            main  =  f'''From {input_date} in the past {period[0]} days, the index of USD against EMEA currencies {df_2:s} by {df*100:.2f}%.'''

    return main
    
    
def prepare_data():
    import pandas as pd
    import numpy as np
    
    curr = pd.read_csv('../data/commodities/currency.csv').iloc[::-1]
    gdp = pd.read_csv('../data/commodities/recent_gdp.csv')
    curr = curr.fillna(0)
    portion = gdp["portion"]
    cols = curr.columns
    curr['index'] = pow(curr[cols[1]] , -portion[0]) + pow(curr[cols[2]],-portion[1] )+\
    pow(curr[cols[3]],portion[2] ) + pow(curr[cols[4]],portion[3] ) \
    + pow(curr[cols[5]],portion[4]  ) + pow(curr[cols[6]],portion[5] )\
    + pow(curr[cols[7]],portion[6] ) + pow(curr[cols[8]],portion[7]) \
    +pow(curr[cols[9]],portion[8] ) + pow(curr[cols[10]],portion[9] )\
    + pow(curr[cols[11]],portion[10] ) + pow(curr[cols[12]],portion[11] ) \
    + pow(curr[cols[13]],portion[12] ) + pow(curr[cols[14]],portion[13] ) \
    + pow(curr[cols[15]],portion[14]) + pow(curr[cols[16]],portion[15])
        
    curr['5D'] = curr['index'].pct_change(periods=5)
    curr['7D'] = curr['index'].pct_change(periods=7)
    curr['30D'] = curr['index'].pct_change(periods=30)
    return curr
        
        
