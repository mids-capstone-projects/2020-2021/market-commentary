from pandas.tseries.offsets import BMonthEnd
import argparse
import os

from commodities_fx import (generate_EMEA_comments, generate_euro_comments,
                            generate_FX_equity, generate_Sector_comments)

from equity import (implied_volatility_commentary,
                    individual_stocks_commentary,
                    industries_overall_commentary, momentum_commentary,
                    overall_commentary, rally_commentary,
                    relevant_industries_commentary, rsi_commentary,
                    sentiment_commentary)

from fixed_income import (generate_credit_spreads_commentary,
                          generate_municipal_bond_turns_commentary,
                          generate_total_returns_commentary,
                          generate_yield_curve_commentary)

from other.exception_handler import exception_handler
from other.style import style
from utils import fixed_income
from datetime import datetime


@exception_handler
def main(start_day: str, end_day: str, threshold: float, count: int, verbose: bool, correlation: float) -> None:
      
    if verbose:

        #######################################################################################
        # EQUITY EQUITY EQUITY EQUITY EQUITY EQUITY EQUITY EQUITY EQUITY EQUITY EQUITY EQUITY #
        #######################################################################################

        #Call each commentary individually and store results
        eq_s1_c1 = overall_commentary.main(start_day=start_day, end_day=end_day, threshold=threshold)
        eq_s1_c2 = rally_commentary.main(start_day=start_day, end_day=end_day)
        eq_s2_c1 = industries_overall_commentary.main(start_day=start_day, end_day=end_day)
        eq_s2_c2 = relevant_industries_commentary.main(start_day=start_day, end_day=end_day, threshold=threshold)
        eq_s3_c1 = sentiment_commentary.main(start_day=start_day, end_day=end_day)
        eq_s3_c2, _ = momentum_commentary.main(start_day=start_day, end_day=end_day)
        eq_s3_c3, _, _ = implied_volatility_commentary.main(start_day=start_day, end_day=end_day, threshold=threshold)
        eq_s3_c4, _, _ = rsi_commentary.main(day=end_day)
        eq_s4_c1, eq_s4_c2 = individual_stocks_commentary.main(start_day=start_day, end_day=end_day, n=count)

        #Print and format results
        print(style.BLANK)
        print(style.BOLD + 'Overview' + style.END)
        print(eq_s1_c1, eq_s1_c2)
        
        print(style.BLANK)
        print(style.BOLD + 'Industries' + style.END)
        print(eq_s2_c1); print(eq_s2_c2)
        
        print(style.BLANK)
        print(style.BOLD + 'Market Sentiment' + style.END)
        print('-', eq_s3_c1); print('-', eq_s3_c2); print('-', eq_s3_c3); print('-', eq_s3_c4)
        
        print(style.BLANK)
        print(style.BOLD + 'Individual Stocks' + style.END)
        print(eq_s4_c2); print(eq_s4_c1)
        print(style.BLANK)

        #######################################################################################
        # FIXED INCOME FIXED INCOME FIXED INCOME FIXED INCOME FIXED INCOME FIXED INCOME FIXED # 
        #######################################################################################

        last_weekday_curr_month = BMonthEnd().rollforward(end_day)

        if end_day == last_weekday_curr_month.strftime("%Y-%m-%d"):
            cm_yield_curve = generate_yield_curve_commentary(input_date=end_day)
            cm_credit_spreads = generate_credit_spreads_commentary(input_date=end_day)
            cm_total_returns = generate_total_returns_commentary(input_date=end_day)
            cm_municipal_bond_returns = generate_municipal_bond_turns_commentary(input_date=end_day)

            print(style.BLANK)
            print(style.BOLD + "Yield Curve" + style.END)
            print(cm_yield_curve)
            
            print(style.BLANK)
            print(style.BOLD + "Credit Spread" + style.END)
            print(cm_credit_spreads)
            
            print(style.BLANK)
            print(style.BOLD + "Total Return" + style.END)
            print(cm_total_returns)
            
            print(style.BLANK)
            print(style.BOLD + "Municipal Fixed Income - Total Return" + style.END)
            print(cm_municipal_bond_returns)
            print(style.BLANK)

        #######################################################################################
        # COMMODITIES & FX COMMODITIES & FX COMMODITIES & FX COMMODITIES & FX COMMODITIES  FX # 
        #######################################################################################
        
        cm_FX_equity = generate_FX_equity(input_date=end_day)
        cm_euro_comments = generate_euro_comments(input_date=end_day)
        cm_emea_comments = generate_EMEA_comments(input_date=end_day)
        cm_Sector_comments = generate_Sector_comments(input_date=end_day)

        print(style.BLANK)
        print(style.BOLD + "U.S. Dollar and Equities" + style.END)
        print(cm_FX_equity)
        
        print(style.BLANK)
        print(style.BOLD + "Dollar VS EMEA Basket of Currencies" + style.END)
        print(cm_emea_comments)
        
        print(style.BLANK)
        print(style.BOLD + "Dollar vs. SPX and Euro vs. SXXP" + style.END)
        print(cm_euro_comments)
        
        
        print(style.BLANK)
        print(style.BOLD + "Sector Summary & Detailed Comments¶" + style.END)
        print(cm_Sector_comments)
        print(style.BLANK)
        print(style.BLANK)

    else:

        eq_s1_c1 = overall_commentary.main(start_day=start_day, end_day=end_day, threshold=threshold)
        eq_s2_c1 = industries_overall_commentary.main(start_day=start_day, end_day=end_day)
        eq_s3_c1 = sentiment_commentary.main(start_day=start_day, end_day=end_day)

        print(style.BLANK)
        print(style.BOLD + 'EQUITY OVERVIEW' + style.END)
        print(eq_s1_c1); print(eq_s2_c1)
        print(eq_s3_c1)
        print(style.BLANK)

    
if __name__=="__main__":
    #Parse command line input
    parser = argparse.ArgumentParser(description='Market Commentary for all Asset Classes')

    #Arguments to be passed to main function
    parser.add_argument('--s',  type=str,
                        help='Day to produce market commentary analysis / Format: yyyy-mm-dd')
    parser.add_argument('--d',  type=str, #default=datetime.today()
                        help='Day to produce market commentary analysis / Format: yyyy-mm-dd')
    parser.add_argument('--t', type=float, default=1,
                        help="Significance threshold for commentary / Format: int")
    parser.add_argument('--n', type=int, default=5,
                        help="Count of observations / Format: int")
    parser.add_argument('--c', type=float, default=0.8,
                        help="Verbosity of output / Format: int")
    parser.add_argument('--v', default=True, action='store_false', 
                        help="Verbosity of output / Format: int")                  

    args = parser.parse_args()

    main(start_day=args.s, end_day=args.d, threshold=args.t,
         count=args.n, verbose=args.v, correlation=args.c)
