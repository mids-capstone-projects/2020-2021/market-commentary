#!/usr/bin/env python
# coding: utf-8


def generate_total_returns_commentary(input_date):

    import calendar
    import json
    import string

    import inflect
    import numpy as np
    import pandas as pd
    from pandas.tseries.offsets import MonthEnd

    p = inflect.engine()
    n2w = p.number_to_words

    dir_path = "../data/fixed_income/ids_fixed_income.json"
    with open(dir_path, encoding="utf-8") as f:
        mapping = json.load(f)

    data_dir = "../data/fixed_income/US Total Return Indices Complete.csv"
    df = pd.read_csv(data_dir, header=None)
    start_date = pd.to_datetime(df.iloc[0, 1], format="%m/%d/%Y")
    end_date = pd.to_datetime("today") if not pd.notna(df.iloc[1, 1]) else df.iloc[1, 1]

    input_date = pd.to_datetime(input_date)
    input_month = input_date.strftime("%B")
    selected_indices = df.iloc[3, 1:].str.replace(" Index", "")
    selected_indices.index = range(len(selected_indices.index))
    selected_indices = selected_indices.dropna()
    selected_indices.index = range(len(selected_indices))
    indices = selected_indices.map(mapping)

    grid = df.iloc[5:, :]
    data = grid.rename(columns=grid.iloc[0]).drop(grid.index[0])
    data.set_index("Dates", inplace=True)
    data.index = pd.to_datetime(data.index)
    data = data.astype(float).round(2)
    df_mtd = data.loc[:, ["MTD" in c for c in data.columns]]
    df_mtd.columns = selected_indices.to_list()
    df_ytd = data.loc[:, ["YTD" in c for c in data.columns]]
    df_ytd.columns = selected_indices.to_list()
    selected_ytd = df_ytd.loc[input_date]
    selected_mtd = df_mtd.loc[input_date]

    text = ""

    highest_mtd_value = selected_mtd.max()
    highest_mtd = indices[selected_ytd.argmax()]
    text += f"""Among these sectors, {highest_mtd} generated the highest total return ({highest_mtd_value:+.2f}%) in the month."""

    idxes_weak_positive = np.where((selected_mtd <= 0.5) & (selected_mtd > 0))
    if idxes_weak_positive[0].tolist():
        text += f""" In addition, {n2w(len(idxes_weak_positive[0]))} of them provide weak positive returns: {", ".join([f"{indices.values[i]} ({selected_mtd[i]:+.2f})" for i in idxes_weak_positive[0]])}."""

    worst_mtd_value = selected_mtd.min()
    worst_mtd = indices[selected_mtd.argmin()]
    if worst_mtd_value < 0:
        text += (
            f""" {worst_mtd} shows the worst performance of {worst_mtd_value:+.2f}%."""
        )

        pos_mtd_neg_ytd = (selected_mtd > 0) & (selected_ytd < 0)
    if np.any(pos_mtd_neg_ytd):
        if pos_mtd_neg_ytd.sum() == 1:
            text += f" However, {indices.values[pos_mtd_neg_ytd][0]} seems to be rising as their returns in the month are positive even though their YTD returns are negative."
        else:
            text += f' However, {", ".join([s for s in indices.values[pos_mtd_neg_ytd]])} seems to be rising as their returns in the month are positive even though their YTD returns are negative.'

    neg_mtd_pos_ytd = (selected_mtd < 0) & (selected_ytd > 0)
    if np.any(neg_mtd_pos_ytd):
        if pos_mtd_neg_ytd.sum() == 1:
            text += f" On the other hand, {indices.values[neg_mtd_pos_ytd][0]} seems to be decreasing as their returns in the month are negative."
        else:
            text += f' On the other hand, {", ".join([s for s in indices.values[neg_mtd_pos_ytd]])} seems to be decreasing as their returns in the month are negative.'

    if input_date.month != 1:
        prev_month = input_date.month - 1
        mrange = calendar.monthrange(input_date.year, prev_month)
        start_date = pd.to_datetime(f"{prev_month}-{mrange[1]}-{input_date.year}")
    else:
        prev_month = 12
        prev_year = input_date.year - 1
        mrange = calendar.monthrange(prev_year, prev_month)
        start_date = pd.to_datetime(f"{prev_month}-{mrange[1]}-{prev_year}")

    mask = pd.date_range(start_date, input_date, freq="BM")
    # print(indices)
    conti_neg = np.all(df_ytd.loc[mask, :] < 0, axis=0)

    if np.all(conti_neg):
        text += (
            f" All sectors' YTD returns are negative for the second consecutive month."
        )
    elif np.all(~conti_neg):
        text += (
            f" All sectors' YTD returns are positive for the second consecutive month."
        )
    elif np.any(conti_neg):
        text += f' Sectors with continuous negative YTD returns since the month before: {", ".join([s for s in indices.values[conti_neg]])}.'
    else:
        pass

    both_negatives = (selected_mtd < 0) & (selected_ytd < 0)
    if sum(both_negatives):
        if sum(both_negatives) == 1:
            text += f""" {indices.values[both_negatives][0]} is only sector presents negative returns for both the month and YTD values."""
        else:
            text += f""" {", ".join([s for s in indices.values[both_negatives]])} are {n2w(sum(both_negatives))} sectors present negative returns for both the month and YTD values."""
    else:
        text += f""" There are none present negative returns for both the month and YTD values."""

    return text
