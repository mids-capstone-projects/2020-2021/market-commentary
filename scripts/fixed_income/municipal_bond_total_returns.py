#!/usr/bin/env python
# coding: utf-8


def generate_municipal_bond_turns_commentary(input_date):

    import json
    import re
    import string

    import numpy as np
    import pandas as pd

    dir_path = "../data/fixed_income/ids_fixed_income.json"
    with open(dir_path, encoding="utf-8") as f:
        mapping = json.load(f)

    data_dir = "../data/fixed_income/Municipal Bonds Total Return Complete.csv"

    df = pd.read_csv(data_dir, header=None)
    start_date = pd.to_datetime(df.iloc[0, 1], format="%m/%d/%Y")
    end_date = today = (
        pd.to_datetime("today") if not pd.notna(df.iloc[1, 1]) else df.iloc[1, 1]
    )

    input_date = input_date
    input_date = pd.to_datetime(input_date)

    input_month = input_date.strftime("%B")

    assert start_date <= input_date <= end_date, "Invalid input date detected."

    selected_indices = df.iloc[3, 1:].str.replace(" Index", "")
    selected_indices.index = range(len(selected_indices.index))

    selected_indices = selected_indices.dropna()
    selected_indices.index = range(len(selected_indices))

    indices = selected_indices.map(mapping)

    grid = df.iloc[5:, :]
    data = grid.rename(columns=grid.iloc[0]).drop(grid.index[0])
    data.set_index("Dates", inplace=True)
    data.index = pd.to_datetime(data.index)
    data = data.astype(float).mul(100).round(2)

    mtd = data.loc[:, ["MTD" in c for c in data.columns]]
    mtd.columns = selected_indices.to_list()
    ytd = data.loc[:, ["YTD" in c for c in data.columns]]
    ytd.columns = selected_indices.to_list()

    current_ytd = ytd.loc[input_date]
    current_mtd = mtd.loc[input_date]

    text = ""

    comparison_mtd = current_mtd.iloc[range(3, 6)]

    ### Long, Mid, Short Comparison

    trim = lambda s: re.sub(r"\([^()]*\)", "", s)

    def make_and_sentence(sr) -> str:
        mapped = sr.index.map(mapping).to_series().apply(trim).tolist()
        return f"{mapped[0]}, {mapped[1]} and {mapped[2]}"

    if (comparison_mtd > 0).all():
        text += f"All segments of {make_and_sentence(comparison_mtd)} are positive."
        sorted_args = comparison_mtd.argsort()
        groups = []

        for i in range(2):
            groups.append((sorted_args.index[i], sorted_args.index[i + 1]))
        for i, g in enumerate(groups):
            l, s = g[0], g[1]
            if not i:
                text += (
                    f"{trim(mapping[l]).title()} outpaced {trim(mapping[s]).lower()}."
                )
            else:
                text += f" Similarly, {trim(mapping[l]).lower()} outperformed {trim(mapping[s]).lower()}."
    elif (comparison_mtd < 0).all():
        text += f"All segments of {make_and_sentence(comparison_mtd)} are negative."

    else:
        cut_off = 0
        pos = comparison_mtd.index[comparison_mtd >= cut_off].tolist()
        neg = comparison_mtd.index[comparison_mtd < cut_off].tolist()

        if len(pos) == 1:
            text += f"Only {trim(mapping[pos[0]]).lower()} were able to achieve a positive return in {input_month}. Both {trim(mapping[neg[0]]).lower()} and {trim(mapping[neg[1]]).lower()} segments fell into negative territory."
        elif len(neg) == 1:
            text += f"Both {trim(mapping[pos[0]]).lower()} and {trim(mapping[pos[1]]).lower()} segments were able to achieve positive returns in {input_month}. Only {trim(mapping[neg[0]]).lower()} fell into negative territory."

    ### Quality Comparison

    AAA_index = 6
    BBB_index = 9
    if (current_mtd[AAA_index] - current_mtd[BBB_index]) > 0:
        text += f" BBBs lagged the return of AAAs by {(current_mtd[AAA_index] - current_mtd[BBB_index]).round():.0f} bps."
    else:
        text += f" AAAs lagged the return of BBBs by {(current_mtd[BBB_index] - current_mtd[AAA_index]).round():.0f} bps."

    # ### High Yield

    idx = "LMHYTR"

    def check_high_yield_ytd():
        if current_mtd[10] > 0:
            return f"risen {(current_ytd[10]/100).round().astype(int)}%"
        else:
            return f"fallen {abs((current_ytd[10]/100).round()).astype(int)}%"

    if abs((current_ytd[10] / 100).round()).astype(int) > 5:
        text += f" YTD, the High Yield municipal category has {check_high_yield_ytd()}."

    return text
