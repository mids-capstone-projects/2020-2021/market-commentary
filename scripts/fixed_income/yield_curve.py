#!/usr/bin/env python
# coding: utf-8


def generate_yield_curve_commentary(input_date):

    import json
    import string
    from datetime import datetime

    import numpy as np
    import pandas as pd

    dir_path = "../data/fixed_income/ids_fixed_income.json"
    with open(dir_path, encoding="utf-8") as f:
        mapping = json.load(f)

    collapse = "monthly"

    data_dir = "../data/fixed_income/US Treasury Yields Complete.csv"

    df = pd.read_csv(data_dir, header=None)
    start_date = pd.to_datetime(df.iloc[0, 1], format="%m/%d/%Y")
    end_date = today = (
        pd.to_datetime("today") if not pd.notna(df.iloc[1, 1]) else df.iloc[1, 1]
    )

    input_date = pd.to_datetime(input_date)

    selected_indices = df.iloc[3, 1:].str.replace(" Index", "")
    selected_indices.index = range(len(selected_indices.index))

    selected_indices

    indices = selected_indices.map(mapping)

    indices

    grid = df.iloc[5:, :]
    data = grid.rename(columns=grid.iloc[0]).drop(grid.index[0])
    data.set_index("Dates", inplace=True)
    data = data.astype(float)
    data.index = pd.to_datetime(data.index)

    data.columns = indices.tolist()

    selected_maturities = ["30 Years", "10 Years", "2 Years"]

    monthly = data.resample("BM").aggregate(lambda x: x[-1])

    one_month_change = monthly.diff(periods=1).loc[input_date]

    yields = data.loc[input_date]

    yields_diff_30yr, yields_diff_10yr, yields_diff_2yr = np.round(
        one_month_change[selected_maturities].to_numpy() * 100, 0
    )

    def num2trend(num):
        if num > 0:
            candidate_words = ["increased", "rose"]
        else:
            candidate_words = ["declined", "decreased", "fell"]
        return np.random.choice(candidate_words)

    text = ""
    text += f"The benchmark 10yr Treasury yield {num2trend(yields_diff_10yr)} {abs(yields_diff_10yr):.0f} bps this {collapse.replace('ly','')}, ending at {yields['10 Years']:.2f}%."

    if np.all(one_month_change > 0):
        text += " In addition, the rates at all maturities are rising."
    elif np.all(one_month_change < 0):
        text += " In addition, the rates at all maturities are falling."

    # text += f'The U.S. Treasury yield curve was basically unchanged throughout the {q2word[mm]} quarter.'
    text += f" At the shorter end of the yield curve, the yield on the 2-year U.S. Treasury note {num2trend(yields_diff_2yr)} {abs(yields_diff_2yr):.0f} basis points to yield {yields['2 Years']:.2f}% at {collapse.replace('ly','')} end."
    text += f" At the longer end of the yield curve, the 30-year U.S.Treasury bond {num2trend(yields_diff_30yr)} {abs(yields_diff_30yr):.0f} basis points to yield {yields['30 Years']:.2f}% at {collapse.replace('ly','')} end."

    return text

    # ### Treasury Yield Curve

    # plot_yields(df)
