#!/usr/bin/env python
# coding: utf-8


def generate_credit_spreads_commentary(input_date):

    import calendar
    import json
    import string
    from datetime import datetime

    import numpy as np
    import pandas as pd


    dir_path = "../data/fixed_income/ids_fixed_income.json"
    with open(dir_path, encoding="utf-8") as f:
        mapping = json.load(f)

    data_dir = "../data/fixed_income/Credit Spreads Indices Complete.csv"

    df = pd.read_csv(data_dir, header=None)
    start_date = pd.to_datetime(df.iloc[0, 1], format="%m/%d/%Y")
    end_date = pd.to_datetime("today") if not pd.notna(df.iloc[1, 1]) else df.iloc[1, 1]

    input_date = input_date
    input_date = pd.to_datetime(input_date)


    assert start_date <= input_date <= end_date, "Invalid input date detected."


    translator = str.maketrans("", "", string.punctuation)
    selected_indices = df.iloc[3, 1:].str.replace(" Index", "")
    selected_indices.index = range(len(selected_indices.index))


    assert selected_indices.str.contains(
        "|".join(mapping.keys()), regex=True
    ).all(), "Invalid data given."


    indices = selected_indices.map(mapping)


    grid = df.iloc[5:, :]
    data = grid.rename(columns=grid.iloc[0]).drop(grid.index[0])


    data.set_index("Dates", inplace=True)
    data = data.astype(float)
    data.index = pd.to_datetime(data.index)
    data = data.mul(100)

    data.columns = indices.tolist()


    data = data.round(0).astype(int)


    targeted_date = data.loc[input_date]


    monthly = data.resample("BM").aggregate(lambda x: x[-1])
    monthly_diff = monthly.diff(periods=1)
    one_month_change = monthly_diff.loc[input_date]


    quarterly = monthly.diff(periods=3)
    one_quarter_change = quarterly.loc[input_date]


    def judge(x: float) -> str:
        if x > 0:
            return f"widened {x:.0f}"
        else:

            return f"tightened {-x:.0f}"

    indices.index = range(len(indices.index))

    # Initialize credit spread example commentary
    text = ""

    # Investment Grade Corporates
    idx = selected_indices.index[selected_indices == "LUACTRUU"].item()
    text += f"{indices[idx]} spreads {judge(one_month_change[idx])} bps in the month and {judge(one_quarter_change[idx])} bps for the quarter to end at {targeted_date[idx]:.0f} bps."

    # U.S. High Yield Corporates, Emerging Market Debt
    idx = selected_indices.index[selected_indices == "LF98TRUU"].item()
    text += f" At the same time, {indices[idx]} {judge(one_month_change[idx])} bps in the month ({judge(one_quarter_change[idx])} for the quarter) to {targeted_date[idx]:.0f} bps."

    # High-Quality securitized sectors: Agency Pass-throughs, Asset-Backed
    idx = selected_indices.index[selected_indices == "LUABTRUU"].item()
    text += f" {indices[idx]} {judge(one_month_change[idx])} bps to {targeted_date[idx]:.0f}."

    idx = selected_indices.index[selected_indices == "I29527US"].item()
    text += f" {indices[idx]} ended the month at {targeted_date[idx]:.0f} bps."

    # Spreads for the past month & widened/tightened spreads(pos/neg) monthly change
    # Spreads further widened/tightened or reversed from the past month and the month before
    if input_date.month >= 2:
        prev_month = input_date.month - 1
        mrange = calendar.monthrange(input_date.year, prev_month)
        start_date = pd.to_datetime(f"{prev_month}-{mrange[1]}-{input_date.year}")
    else:
        prev_month = 12
        prev_year = input_date.year - 1
        mrange = calendar.monthrange(prev_year, prev_month)
        start_date = pd.to_datetime(f"{prev_month}-{mrange[1]}-{prev_year}")

    mask = pd.date_range(start_date, input_date, freq="BM")

    consecutive_values = monthly_diff.loc[mask, :]

    further_widening = np.all(consecutive_values > 0, axis=0)

    if np.all(further_widening):
        text += f" All sectors have been widened for the second consecutive month."
    elif np.all(~further_widening):
        text += f" All sectors have been tightened for the second consecutive month."
    elif np.any(further_widening):
        if sum(further_widening) == 1:
            if consecutive_values.values[:, further_widening].sum() > 1:
                text += f" Sector of {indices.values[further_widening][0]} has been widened for the second consecutive month ({consecutive_values.values[:,further_widening].sum():+.0f} bps)."
            else:
                text += f" Sector of {indices.values[further_widening][0]} has been widened for the second consecutive month ({consecutive_values.values[:,further_widening].sum():+.0f} bp)."
        else:
            text += f" Sectors that haven been widened for the second consecutive month are:\n"
            for x in indices.values[further_widening]:
                text += f"\t-{x}\n"

    # When sectors widened/tightened across all sectors, identify the top two which had the reatest(pos/neg) monthly change

    # Sectors that widened/tightened against overall trend of other sectors

    # ### Sectors with YTD spread tightening/widening
    return text
