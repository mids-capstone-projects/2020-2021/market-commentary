import json


# Fetch
def load_indices(path:str) -> dict:
    '''
    Return the specified indices to generate look up table
    '''
    with open(path, encoding='utf-8') as f:
        data = json.load(f)

    return data


if __name__ == '__main__':
    pass