import pandas as pd
import os

def american_industries():
    #Dictionary of ticker names
    SPX_DICT = {'S5COND': 'Consumer Discretionary', 'S5CONS': 'Consumer Staples', 'S5ENRS': 'Energy', 'S5FINL': 'Financials',
                'S5HLTH': 'Healthcare', 'S5INDU': 'Industrials', 'S5INFT': 'Technology', 'S5MATR': 'Materials', 
                'S5RLST': 'Real Estate', 'S5TELS': 'Telecommunications', 'S5UTIL': 'Utilities'}

    path = os.path.join(os.getcwd(), '..', 'data', 'equity', 'Industries', 'SPX')

    SPX = pd.read_csv(os.path.join(os.getcwd(), '..', 'data', 'equity', 'SPX.csv'))
    SPX['Dates'] =  pd.to_datetime(SPX['Dates'], format='%m/%d/%Y')

    SPX_Volume = pd.DataFrame(SPX['Dates'])
    SPX_Price  = pd.DataFrame(SPX['Dates'])

    #Loop through every csv file and extract volume and price
    for filename in os.listdir(os.path.join(path)):
        #pdb.set_trace()
        with open(os.path.join(path, filename)) as file:
            df = pd.read_csv(file)

            # Write industries Volume and Price
            SPX_Volume[SPX_DICT[filename[:-4]]] = df['PX_VOLUME']
            SPX_Price[SPX_DICT[filename[:-4]]] = df['PX_LAST']
    
    return SPX_Price

def european_industries():
    pass