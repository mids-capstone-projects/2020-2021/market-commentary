### Multiple functions that can be reused across commentaries
from   typing import Tuple, List, Dict
import numpy as np

def uncertainty_region(value: float, quartiles: List[float]) -> str:
    if value > quartiles[3]:
        return 'high', 2
    elif value > quartiles[2]:
        return 'neutral-high', 1
    elif value > quartiles[1]:
        return 'neutral', 0
    elif value > quartiles[0]:
        return 'neutral-low', -1
    else:
        return 'low', -2

def uncertainty_significance(change: float, zscore: float, threshold: int = 1) -> str:
    #Direction of movement
    sign = 'upward' if change > 0 else 'downward'

    #Significance of movement in terms of threshold
    if np.abs(zscore) >= threshold:
        significance = 'sharp'
    elif np.abs(zscore) >= threshold*0.5:
        significance = 'standard'
    else:
        significance = 'mild'
    
    return sign, significance