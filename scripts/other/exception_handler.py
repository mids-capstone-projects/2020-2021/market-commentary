# Exception Handler to deal with the most frequent errors and provide meaningful messages
import datetime
from other.style import style

def exception_handler(function):
    def inner_function(*args, **kwargs):
        try:
            return function(*args, **kwargs)

        except:
            #Capturing all function arguments in local environment
            arguments = locals()

            #Assert whether values were provided for dates
            assert kwargs['start_day'] is not None, style.RED + '''Please provide a start date.''' + style.END
            assert kwargs['end_day'] is not None, style.RED + '''Please provide an end date.''' + style.END

            #Check start date is provided in the correct format
            try:
                start_day = datetime.datetime.strptime(kwargs['start_day'], '%Y-%m-%d')
            except ValueError:
                raise ValueError(style.RED + "Incorrect date format for start day. Please enter the correct day in YYYY-MM-DD format." + style.END)

            #Check end date is provided in the correct format
            try:
                end_day = datetime.datetime.strptime(kwargs['end_day'], '%Y-%m-%d')
            except ValueError:
                raise ValueError(style.RED + "Incorrect date format for start day. Please enter the correct day in YYYY-MM-DD format." + style.END)

            #Checking whether the error is because of a weekend
            
            assert(start_day.weekday() <= 4), style.RED + '''This function only accepts weekdays. Please enter the correct start date in YYYY-MM-DD format.''' + style.END
            assert(end_day.weekday() <= 4), style.RED + '''This function only accepts weekdays. Please enter the correct end date in YYYY-MM-DD format.''' + style.END

            #Checking whether the start date is greater than the end date
            assert(start_day < end_day), style.RED + '''Please make sure the start date precedes the end date.''' + style.END

            return style.RED + "Unknown error. Please try again with other inputs." + style.END
            
    return inner_function